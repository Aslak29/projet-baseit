<?php
require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeArticle = $objBdd->query("SELECT * FROM article
                                        JOIN user ON article.idUser = user.idUser
                                        ORDER BY datePub DESC 
                                        LIMIT 0,5");
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php $titre = "Acceuil"; ?>
<?php ob_start();
session_start();
?>
<article>
    <h1> Les 5 derniers article publiés</h1>

    <ul>
        <?php foreach ($listeArticle as $article) { ?>
            <li>
                <a href="article.php?idArticle=<?= $article['idArticle']; ?>">
                    <span> <?= $article['titre']; ?> <?php echo " - Auteur : " . $article['pseudo'] . " - date : " . $article['datePub']; ?></span></a>

            </li>
        <?php } ?>

    </ul>

</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>