<?php if ((!isset($_POST['idArticle']))) {
    // Code de supprBassin 
?>
    <?php $titre = "Supprimer Article"; ?>
    <?php ob_start(); ?>

    <?php

    //Requete SQL
    require "bdd/bddconfig.php";
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $listeArticle = $objBdd->query("select * from article");
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
    ?>
    <article>
        <?php
        session_start();
        //Accès seulement si authentifié 
        if (isset($_SESSION['logged_in']['login']) !== TRUE) {
            // Redirige vers la page d'accueil (ou login.php) si pas authentifié
            $serveur = $_SERVER['HTTP_HOST'];
            $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
            $page = 'index.php';
            header("Location: http://$serveur$chemin/$page");
        }
        ?>
        <h1>Les Articles :</h1>
        <table>
            <thead>
                <tr>
                    <th>Nom <Article></Article>
                    </th>
                    <th>Supprimer</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($listeArticle as $article) { ?>
                    <tr>
                        <td><?php echo $article['titre']; ?></td>
                        <td>
                            <form method="POST" action="supprbassin.php">
                                <input type="hidden" name="idArticle" value="<?php echo $article['idArticle']; ?>">
                                <input type="submit" value="Supprimer">
                            </form>
                        </td>
                    </tr>
                <?php
                } //fin foreach
                $article->closeCursor(); //libère les ressources de la bdd
                ?>
            </tbody>
        </table>
    </article>


    <?php $contenu = ob_get_clean(); ?>
    <?php require 'gabarit/template.php'; ?>
<?php } else {
    // Code de deletebassin 
?>
    <?php
    require "bdd/bddconfig.php";

    $paramOK = false;
    // Recup la variables POST et les sécurise
    if ((isset($_POST['idArticle']))) {
        $idBassin = intval(htmlspecialchars($_POST['idArticle']));
        $paramOK = true;
    }

    // INSERT dans la base
    if ($paramOK == true) {
        try {
            $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
            $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Supprimer les article
            $delBassin = $objBdd->prepare("DELETE FROM article WHERE idArticle =:id");
            $delBassin->bindParam(':id', $idArticle, PDO::PARAM_INT);
            $delBassin->execute();

            //Supprimer le bassin de la table bassin
            $delBassin = $objBdd->prepare("DELETE FROM document WHERE idArticle =:id");
            $delBassin->bindParam(':id', $idArticle, PDO::PARAM_INT);
            $delBassin->execute();


            // Redirige vers une page différente du dossier courant
            $serveur = $_SERVER['HTTP_HOST'];
            $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $page = 'bassins.php';

            header("Location: http://$serveur$chemin/$page");
            //header("Location: http://localhost:82/formulaires/index.php");
        } catch (Exception $prmE) {
            die('Erreur : ' . $prmE->getMessage());
        }
    } else {
        die('Les paramètres reçus ne sont pas valides');
    } ?>
<?php } ?>