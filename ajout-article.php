<?php $titre = "Ajouter un article"; ?>
<?php ob_start(); ?>

<article>
    <?php
    session_start();
    //Accès seulement si authentifié 
    if (isset($_SESSION['logged_in']['login']) !== TRUE) {
        // Redirige vers la page d'accueil (ou login.php) si pas authentifié
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    }
    ?>
    <h1>Ajouter un Article</h1>
    <form method="POST" action="insertarticle.php">
        <fieldset>
            <legend>Ajout d'un article</legend>
            Nom :<br />
            <input type="text" name="titre" value="" placeholder="Titre de l'article" required>
            <br />
            Description :<br>
            <textarea name="texte" rows="10" cols="40" placeholder="Texte de l'article" required></textarea>
            <br />
            <input type="submit" value="Enregistrer">
        </fieldset>
    </form>
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>